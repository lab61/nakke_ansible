
nakke-dev: nakke-dev
	ansible-playbook ./nakke-dev.yml \
	--inventory ./hosts.yaml \
	--ask-vault-pass --ask-become-pass --diff

deps:
	ansible-galaxy install -r ./requirements.yml
